package com.blizzard.api;

import java.util.List;

import com.blizzard.api.beans.ApiResponse;
import com.blizzard.api.beans.Player;
import com.blizzard.api.service.BlizzardService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController()
@RequestMapping("/api")
public class Controller {

    @Inject
    private BlizzardService blizzardService;

    @GetMapping(value = "/players", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<Player> getAllPlayers() {
        return blizzardService.getAllPlayers();
    }

    @GetMapping(value = "/players/{playerId}/ingameContent", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<String> getIngameContent(@PathVariable("playerId") int playerId) {
        return blizzardService.getIngameContent(playerId);
    }

    @PutMapping(value = "/players/{playerId}/ingameContent/{ingameContentId}",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ApiResponse purchaseIngameContent(@PathVariable("playerId") int playerId,
                                             @PathVariable("ingameContentId") int ingameContentId) {
        return blizzardService.purchaseIngameContent(playerId, ingameContentId);
    }

}