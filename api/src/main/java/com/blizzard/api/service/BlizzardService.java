package com.blizzard.api.service;

import com.blizzard.api.beans.ApiResponse;
import com.blizzard.api.beans.Player;
import com.blizzard.api.repository.BlizzardRepo;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@Service
public class BlizzardService {

    @Inject
    private BlizzardRepo repository;

    public List<Player> getAllPlayers() {
        return repository.getAllPlayers();
    }

    public List<String> getIngameContent(int playerId) {
        return repository.getIngameContent(playerId);
    }

    public ApiResponse purchaseIngameContent(int playerId, int ingameContentId) {
        return repository.purchaseIngameContent(playerId, ingameContentId);
    }
}
