package com.blizzard.api.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {

    public static final String DB_JDBC_TEMPLATE = "dbJdbcTemplate";

    public DatabaseConfig() {
    }

    @Bean(name = {"dbDataSource"})
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource dbDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = {"dbJdbcTemplate"})
    public NamedParameterJdbcTemplate dbJdbcTemplate() {
        return new NamedParameterJdbcTemplate(this.dbDataSource());
    }
}
