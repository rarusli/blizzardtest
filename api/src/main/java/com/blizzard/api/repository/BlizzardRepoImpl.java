package com.blizzard.api.repository;

import com.blizzard.api.beans.ApiResponse;
import com.blizzard.api.beans.Player;
import com.blizzard.api.config.DatabaseConfig;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Repository
public class BlizzardRepoImpl implements BlizzardRepo {

    private static final String GET_ALL_PLAYERS =
            "SELECT id, first_name, last_name, email FROM player";

    private static final String GET_INGAME_CONTENT =
            "SELECT player.id, first_name, last_name, email, name FROM \n" +
                    "player, player_ingame_content, ingame_content\n" +
                    "WHERE \n" +
                    "player.id=:id AND\n" +
                    "player.id=player_ingame_content.player_id AND\n" +
                    "player_ingame_content.ingame_content_id=ingame_content.id";

    private static final String PURCHASE_INGAME_CONTENT =
            "MERGE INTO player_ingame_content AS t USING (VALUES(:playerId,:ingameContentId)) AS vals(a,b)\n" +
                    "    ON t.player_id=vals.a AND t.ingame_content_id=vals.b\n" +
                    "    WHEN MATCHED THEN UPDATE SET t.player_id=vals.a, t.ingame_content_id=vals.b\n" +
                    "    WHEN NOT MATCHED THEN INSERT VALUES vals.a, vals.b";

    @Resource(name= DatabaseConfig.DB_JDBC_TEMPLATE)
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public List<Player> getAllPlayers() {

        List<Player> players = jdbcTemplate.query(GET_ALL_PLAYERS,
                new ResultSetExtractor<List<Player>>() {
                    @Override
                    public List<Player> extractData(ResultSet rs) throws SQLException,
                            DataAccessException {

                        List<Player> players = new ArrayList<>();

                        while (rs.next()) {
                            Player player = new Player();
                            player.setId(rs.getInt("id"));
                            player.setFirstName(rs.getString("first_name"));
                            player.setLastName(rs.getString("last_name"));
                            player.setEmail(rs.getString("email"));
                            players.add(player);
                        }

                        return players;
                    }
                });

        return players;
    }

    @Override
    public List<String> getIngameContent(int playerId) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", playerId);

        List<String> ingameContent = jdbcTemplate.query(GET_INGAME_CONTENT, params,
                new ResultSetExtractor<List<String>>() {
                    @Override
                    public List<String> extractData(ResultSet rs) throws SQLException,
                            DataAccessException {

                        List<String> ingameContent = new ArrayList<>();

                        while (rs.next()) {
                            ingameContent.add(rs.getString("name"));
                        }

                        return ingameContent;
                    }
                });

        return ingameContent;
    }

    @Override
    public ApiResponse purchaseIngameContent(int playerId, int ingameContentId) {

        Map<String, Object> params = new HashMap<>();
        params.put("playerId", playerId);
        params.put("ingameContentId", ingameContentId);

        int count = jdbcTemplate.update(
                PURCHASE_INGAME_CONTENT,
                params);

        System.out.println("count:" + count);

        if (count>0) {
            return new ApiResponse(HttpStatus.OK, "# of message procssed " + count);
        }

        return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, "# of message procssed " + count);
    }

}
