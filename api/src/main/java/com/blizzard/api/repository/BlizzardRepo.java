package com.blizzard.api.repository;

import com.blizzard.api.beans.ApiResponse;
import com.blizzard.api.beans.Player;

import java.util.List;

public interface BlizzardRepo {

    List<Player> getAllPlayers();

    List<String> getIngameContent(int playerId);

    ApiResponse purchaseIngameContent(int playerId, int ingameContentId);
}
