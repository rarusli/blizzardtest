### Introduction

This project is composed of 2 parts:
(1) a database (HsqlDB)
(3) a rest API frontend (Spring Boot)

This is a simple exercise to build a simple end to end ecommerce system (database + api) with the following basic
capabilities : 
- shows list of players in the system
- list of ingame-content that a player has 
- ability for player to purchase ingame-content

### How to run

Start the database (blizzard_db):
- cd db
 - java -cp ./hsqldb-2.5.1.jar org.hsqldb.server.Server --database.0 file:test/db --dbname.0 blizzard_db

Start the API:
 - cd api
 - mvn clean compile spring-boot:run
 
### API design

Swagger/ API documentation is available (after running the API per instruction above) <br />
http://localhost:8080/swagger-ui.html

| Method | Address | Purpose |
| ---      |  ------  |----------|
| GET  | /api/players   | Get all players   |
| GET  | /api/players/{playerId}/ingameContent  | Get ingame content for a given player   |
| PUT  | /api/players/{playerId}/ingameContent/{ingameContentId} | Purchase an ingame content id for a given player |
 
### Database design

![Database design](img/db_design.png "HSQL design")

There are some prepopulated data that you can use to test

```sql
INSERT INTO player (first_name, last_name, email) VALUES ('jane', 'doe', 'jane.doe@hotmail.com');
INSERT INTO player (first_name, last_name, email) VALUES ('rudy', 'rusli', 'rrusli@hotmail.com');

INSERT INTO ingame_content (game_id, name, price) VALUES (1, 'Heroic Reinforcement Bundle', 30);
INSERT INTO ingame_content (game_id, name, price) VALUES (1, 'Welcome to the nexus bundle', 45);

INSERT INTO game(name) values('World of Warcraft');
INSERT INTO game(name) values('Heroes of the Storm');
```

### Sample input and output

###### **Get all players**

curl -X GET "http://localhost:8080/api/players" -H  "accept: application/json"

```json
[
  {
    "id": 0,
    "firstName": "jane",
    "lastName": "doe",
    "email": "jane.doe@hotmail.com"
  },
  {
    "id": 1,
    "firstName": "rudy",
    "lastName": "rusli",
    "email": "rrusli@hotmail.com"
  }
]
```

###### **Get ingame content for a given player**

curl -X GET "http://localhost:8080/api/players/0/ingameContent" -H  "accept: application/json"

```json
[
  "Heroic Reinforcement Bundle",
  "Welcome to the nexus bundle"
]
```

###### **Purchase an ingame content id for a given player**

curl -X PUT "http://localhost:8080/api/players/1/ingameContent/0" -H  "accept: application/json"

```json
{
  "httpStatus": "OK",
  "message": "# of message procssed 1"
}
```
